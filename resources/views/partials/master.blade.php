@include('partials.header')


<!-- Site wrapper -->

  <!-- Navbar -->
      @include('partials.nav')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    @include('partials.sidebar')
    @include('partials.content-header')
  @include('partials.main')
  
  @include('partials.footer')
  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
<!-- ./wrapper -->

@include('partials.end')
